# IOBOARD

KiCAD Files for the hardware design of the PCB. 

The board connects to the GPIO ports of the ODROID H2 SBC, for now just schematics are available, as PCB design will depend on form factor of main hardware and case design.

The firmware is available at https://gitlab.com/thedrastical/iuvia/ioboard-firmware
